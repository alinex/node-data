import { expect } from 'chai';

import { merge } from '../../src/index';

describe('merge', () => {
    it('should support default settings', function() {
        const result = merge(
            {
                data: {
                    name: 'Unknown',
                    hair: {
                        color: 'brown'
                    },
                    family: ['mom', 'dad']
                },
                source: 'x',
                map: true
            },
            {
                data: {
                    name: 'Mark',
                    gender: 'Male',
                    hair: {
                        cut: 'short'
                    },
                    family: ['wife']
                },
                source: 'y',
                map: true
            }
        );
        expect(result.data).to.deep.equal({
            name: 'Mark',
            gender: 'Male',
            hair: {
                color: 'brown',
                cut: 'short'
            },
            family: ['mom', 'dad', 'wife']
        });
        expect(result.map).to.deep.equal({
            __map: ['x', ''],
            name: { __map: ['y', 'name'] },
            hair: {
                __map: ['x', 'hair'],
                color: { __map: ['x', 'hair.color'] },
                cut: { __map: ['y', 'hair.cut'] }
            },
            family: {
                '0': { __map: ['x', 'family.0'] },
                '1': { __map: ['x', 'family.1'] },
                '2': { __map: ['y', 'family.0'] },
                __map: ['x', 'family']
            },
            gender: { __map: ['y', 'gender'] }
        });
    });
    // data: 1, base: 'a.b' => { a: { b: 1 } }
    it('should support base', function() {
        const result = merge({ data: 1, base: 'a.b', source: 'x', map: true });
        expect(result.data).to.deep.equal({ a: { b: 1 } });
        expect(result.map).to.deep.equal({
            a: { b: { __map: ['x', 'a.b'] }, __map: ['x', 'a'] },
            __map: ['x', '']
        });
    });

    describe('object', () => {
        // {a:1, b:2} + {b:4, e:5} => {a:1, b:4, e:5}
        it('should support object combine', function() {
            const result = merge(
                { data: { a: 1, b: 2 }, source: 'x', map: true },
                { data: { b: 4, e: 5 }, source: 'y', object: `combine`, map: true }
            );
            expect(result.data).to.deep.equal({ a: 1, b: 4, e: 5 });
            expect(result.map).to.deep.equal({
                a: { __map: ['x', 'a'] },
                b: { __map: ['y', 'b'] },
                e: { __map: ['y', 'e'] },
                __map: ['x', '']
            });
        });
        // {a:1, b:2} + {b:4, e:5} => {b:4, e:5}
        it('should support object replace', function() {
            const result = merge(
                { data: { a: 1, b: 2 }, source: 'x', map: true },
                { data: { b: 4, e: 5 }, source: 'y', object: `replace`, map: true }
            );
            expect(result.data).to.deep.equal({ b: 4, e: 5 });
            expect(result.map).to.deep.equal({
                b: { __map: ['y', 'b'] },
                e: { __map: ['y', 'e'] },
                __map: ['y', '']
            });
        });
        // {a:1, b:2} + {b:4, e:5} => {a:1, b:2, e:5}
        it('should support object missing', function() {
            const result = merge(
                { data: { a: 1, b: 2 }, source: 'x', map: true },
                { data: { b: 4, e: 5 }, source: 'y', object: `missing`, map: true }
            );
            expect(result.data).to.deep.equal({ a: 1, b: 2, e: 5 });
            expect(result.map).to.deep.equal({
                a: { __map: ['x', 'a'] },
                b: { __map: ['x', 'b'] },
                e: { __map: ['y', 'e'] },
                __map: ['x', '']
            });
        });
        // {a:1, b:2} + {b:4, e:5} => {a:1, b:2} but
        it('should keep data on object alternate', function() {
            const result = merge(
                { data: { a: 1, b: 2 }, source: 'x', map: true },
                { data: { b: 4, e: 5 }, source: 'y', object: `alternate`, map: true }
            );
            expect(result.data).to.deep.equal({ a: 1, b: 2 });
            expect(result.map).to.deep.equal({
                a: { __map: ['x', 'a'] },
                b: { __map: ['x', 'b'] },
                __map: ['x', '']
            });
        });
        // {} + {b:4, e:5} => {b:4, e:5}
        it('should replace data on object alternate', function() {
            const result = merge(
                { data: {}, source: 'x', map: true },
                { data: { b: 4, e: 5 }, source: 'y', object: `alternate`, map: true }
            );
            expect(result.data).to.deep.equal({ b: 4, e: 5 });
            expect(result.map).to.deep.equal({
                b: { __map: ['y', 'b'] },
                e: { __map: ['y', 'e'] },
                __map: ['y', '']
            });
        });
    });
    describe('array', () => {
        // [ 1, 2, 3 ] + [ 4, 5] => [ 1, 2, 3, 4, 5 ]
        it('should support array concat', function() {
            const result = merge(
                { data: [1, 2, 3], source: 'x', map: true },
                { data: [4, 5], source: 'y', array: `concat`, map: true }
            );
            expect(result.data).to.deep.equal([1, 2, 3, 4, 5]);
            expect(result.map).to.deep.equal({
                0: { __map: ['x', '0'] },
                1: { __map: ['x', '1'] },
                2: { __map: ['x', '2'] },
                3: { __map: ['y', '0'] },
                4: { __map: ['y', '1'] },
                __map: ['x', '']
            });
        });
        // [ 1, 2, 3 ] + [ 4, 5] => [ 4, 5 ]
        it('should support array replace', function() {
            const result = merge(
                { data: [1, 2, 3], source: 'x', map: true },
                { data: [4, 5], source: 'y', array: `replace`, map: true }
            );
            expect(result.data).to.deep.equal([4, 5]);
            expect(result.map).to.deep.equal({
                0: { __map: ['y', '0'] },
                1: { __map: ['y', '1'] },
                __map: ['y', '']
            });
        });
        // [ 1, 2, 3 ] + [ 4, 5] => [ 4, 5, 3 ]
        it('should support array overwrite', function() {
            const result = merge(
                { data: [1, 2, 3], source: 'x', map: true },
                { data: [4, 5], source: 'y', array: `overwrite`, map: true }
            );
            expect(result.data).to.deep.equal([4, 5, 3]);
            expect(result.map).to.deep.equal({
                0: { __map: ['y', '0'] },
                1: { __map: ['y', '1'] },
                2: { __map: ['x', '2'] },
                __map: ['x', '']
            });
        });
        // [ {a:1}, {b:2} ] + [ {d:4} ] => [ {a:1, d:4}, {b:2} ]
        it('should support array combine', function() {
            const result = merge(
                { data: [{ a: 1 }, { b: 2 }], source: 'x', map: true },
                { data: [{ d: 4 }], source: 'y', array: `combine`, map: true }
            );
            expect(result.data).to.deep.equal([{ a: 1, d: 4 }, { b: 2 }]);
            expect(result.map).to.deep.equal({
                0: {
                    a: { __map: ['x', '0.a'] },
                    d: { __map: ['y', '0.d'] },
                    __map: ['x', '0']
                },
                1: { b: { __map: ['x', '1.b'] }, __map: ['x', '1'] },
                __map: ['x', '']
            });
        });
    });
    // { normal: [ 1, 2, 3 ], specific: [ 1, 2, 3 ] }
    // + { normal: [ 4, 5], specific: [ 4, 5] }
    // => { normal: [ 1, 2, 3, 4, 5 ], specific: [ 4, 5 ] }
    it('should support specific method', function() {
        const result = merge(
            { data: { normal: [1, 2, 3], specific: [1, 2, 3] }, source: 'x', map: true },
            {
                data: { normal: [4, 5], specific: [4, 5] },
                source: 'y',
                pathArray: { specific: 'replace' },
                map: true
            }
        );
        expect(result.data).to.deep.equal({ normal: [1, 2, 3, 4, 5], specific: [4, 5] });
        expect(result.map).to.deep.equal({
            __map: ['x', ''],
            normal: {
                '0': { __map: ['x', 'normal.0'] },
                '1': { __map: ['x', 'normal.1'] },
                '2': { __map: ['x', 'normal.2'] },
                '3': { __map: ['y', 'normal.0'] },
                '4': { __map: ['y', 'normal.1'] },
                __map: ['x', 'normal']
            },
            specific: {
                '0': { __map: ['y', 'specific.0'] },
                '1': { __map: ['y', 'specific.1'] },
                __map: ['y', 'specific']
            }
        });
    });
    // { normal: [ 1, 2, 3 ], specific: [ 1, 2, 3 ] }
    // + { normal: [ 4, 5], specific: [ 4, 5] }
    // => { normal: [ 1, 2, 3, 4, 5 ], specific: [ 4, 5 ] }
    it('should support array flag', function() {
        const result = merge(
            { data: { normal: [1, 2, 3], specific: [1, 2, 3] }, source: 'x', map: true },
            {
                data: { normal: [4, 5], specific: [4, 5, 'MERGE_ARRAY_REPLACE'] },
                source: 'y',
                map: true
            }
        );
        expect(result.data).to.deep.equal({ normal: [1, 2, 3, 4, 5], specific: [4, 5] });
        expect(result.map).to.deep.equal({
            __map: ['x', ''],
            normal: {
                '0': { __map: ['x', 'normal.0'] },
                '1': { __map: ['x', 'normal.1'] },
                '2': { __map: ['x', 'normal.2'] },
                '3': { __map: ['y', 'normal.0'] },
                '4': { __map: ['y', 'normal.1'] },
                __map: ['x', 'normal']
            },
            specific: {
                '0': { __map: ['y', 'specific.0'] },
                '1': { __map: ['y', 'specific.1'] },
                __map: ['y', 'specific']
            }
        });
    });
    describe('dependencies', () => {
        it('should work with alternate', function() {
            const result = merge(
                { source: 'a.json', data: {} },
                { source: 'a.yaml', alternate: ['a.json'], data: { a: 'a' } },
                { source: 'a.ini', alternate: ['a.json', 'a.yaml'], data: { a: 'aa' } }
            );
            expect(result.data).to.deep.equal({ a: 'a' });
        });
        it('should work with depend', function() {
            const result = merge(
                { source: 'base', data: { a: 'a' } },
                { source: 'extra', depend: ['base'], data: { b: 'b' } },
                { source: 'default', alternate: ['base'], data: { c: 'c' } }
            );
            expect(result.data).to.deep.equal({ a: 'a', b: 'b' });
        });
        it('should skip with depend', function() {
            const result = merge(
                { source: 'base', data: {} },
                { source: 'extra', depend: ['base'], data: { b: 'b' } },
                { source: 'default', alternate: ['base'], data: { c: 'c' } }
            );
            expect(result.data).to.deep.equal({ c: 'c' });
        });
    });
});
