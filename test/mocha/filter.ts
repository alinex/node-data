import { expect } from "chai";

import { filter, posEndPart } from "../../src/filter";

const example = {
    name: "example",
    empty: "",
    null: null,
    boolean: true,
    string: "test",
    number: 5.6,
    date: new Date("2016-05-10T19:06:36.909Z"),
    list: [1, 2, 3, 4, 5, 6],
    person: {
        name: "Alexander Schilling",
        job: "Developer"
    },
    complex: [{ name: "Egon" }, { name: "Janina", admin: true }]
};

describe("filter", () => {
    describe("element path", () => {
        it("should get root element", async () => {
            expect(filter(example, "")).to.deep.equal(example);
        });
        it("should get named element", async () => {
            expect(filter(example, "name")).to.deep.equal("example");
        });
        it("should get list element using dot notation", async () => {
            expect(filter(example, "list.0")).to.deep.equal(example.list[0]);
        });
        it("should get list element using square brackets", async () => {
            expect(filter(example, "list[1]")).to.deep.equal(example.list[1]);
        });
        it("should get list element using square brackets (negative)", async () => {
            expect(filter(example, "list[-1]")).to.deep.equal(example.list[4]);
        });
        it("should get object element using dot notation", async () => {
            expect(filter(example, "person.name")).to.deep.equal(example.person.name);
        });
        it("should get object element using square brackets", async () => {
            expect(filter(example, "person[name]")).to.deep.equal(example.person.name);
        });
        it("should get object element with spaces", async () => {
            expect(filter({ "with space": true }, "[with space]")).to.deep.equal(true);
        });
    });
    describe("path selection", () => {
        it("should get index list", async () => {
            expect(filter(example, "list[1,2,3]")).to.deep.equal([example.list[1], example.list[2], example.list[3]]);
        });
        it("should get name list", async () => {
            expect(filter(example, "[name,string]")).to.deep.equal([example.name, example.string]);
        });
        it("should get index with range", async () => {
            expect(filter(example, "list[1-3]")).to.deep.equal([example.list[1], example.list[2], example.list[3]]);
        });
        it("should get index with mixed range", async () => {
            expect(filter(example, "list[1-3,5]")).to.deep.equal([
                example.list[1],
                example.list[2],
                example.list[3],
                example.list[5]
            ]);
        });
        it("should get index with multiple ranges", async () => {
            expect(filter(example, "list[1-2,4-5]")).to.deep.equal([
                example.list[1],
                example.list[2],
                example.list[4],
                example.list[5]
            ]);
        });
        it("should get filtered map", async () => {
            expect(filter(example, "{name,string}")).to.deep.equal({
                name: example.name,
                string: example.string
            });
        });
        it("should get complete list", async () => {
            expect(filter(example, "list[]")).to.deep.equal(example.list);
            expect(filter(example, "list.*")).to.deep.equal(example.list);
        });
        it("should get complete map", async () => {
            expect(filter(example, "person[]")).to.deep.equal([example.person.name, example.person.job]);
            expect(filter(example, "person.*")).to.deep.equal([example.person.name, example.person.job]);
        });
        it("should get sub elements", async () => {
            expect(filter(example, "complex[1].name")).to.deep.equal(example.complex[1].name);
        });
        it("should get multiple sub elements", async () => {
            expect(filter(example, "complex[].name")).to.deep.equal([example.complex[0].name, example.complex[1].name]);
            expect(filter(example, "complex.*.name")).to.deep.equal([example.complex[0].name, example.complex[1].name]);
        });
    });
    describe("negative path selection", () => {
        it("should get index list", async () => {
            expect(filter(example, "list[!1,2,3]")).to.deep.equal([example.list[0], example.list[4], example.list[5]]);
        });
        it("should get name list", async () => {
            expect(filter(example, "[!name,string,null,list,number,empty]")).to.deep.equal([
                example.boolean,
                example.date,
                example.person,
                example.complex
            ]);
        });
        it("should get filtered map", async () => {
            expect(filter(example, "{!name,string,null,list,number,empty}")).to.deep.equal({
                boolean: example.boolean,
                date: example.date,
                complex: example.complex,
                person: example.person
            });
        });
    });
    describe("values", () => {
        it("should get text", async () => {
            expect(filter(example, '"test"')).to.deep.equal("test");
        });
        it("should allow escapes", async () => {
            expect(filter(example, '"test \\""')).to.deep.equal('test "');
        });
    });
    describe("term finder", () => {
        it("should work on empty text", async () => {
            expect(posEndPart("")).to.equal(0);
        });
        it("should work with simple text", async () => {
            expect(posEndPart("text")).to.equal(4);
        });
        it("should end on first space", async () => {
            expect(posEndPart("text + text")).to.equal(4);
        });
        it("should ignore space in braces", async () => {
            expect(posEndPart("[text, list] + text")).to.equal(12);
        });
        it("should ignore space in double quotes", async () => {
            expect(posEndPart('[text, "list] "] + text')).to.equal(16);
        });
        it("should work with escapes in quotes", async () => {
            expect(posEndPart('[text, "list]\\" "] + text')).to.equal(18);
        });
    });
    describe("combine", () => {
        it("should go through", async () => {
            expect(filter(example, "list | [!1,2,3]")).to.deep.equal([
                example.list[0],
                example.list[4],
                example.list[5]
            ]);
        });
        it("should go through multiple", async () => {
            expect(filter(example, "complex | [0] | name")).to.deep.equal("Egon");
        });
    });
    describe("alternative", () => {
        it("should use base if defined", async () => {
            expect(filter(example, 'list || "missing"')).to.deep.equal(example.list);
        });
        it("should use alternative", async () => {
            expect(filter(example, 'missing || "missing"')).to.deep.equal("missing");
        });
    });
    describe("concat", () => {
        it("should concat strings into array", async () => {
            expect(filter(example, '"list" , "missing"')).to.deep.equal(["list", "missing"]);
        });
        it("should concat array , string into array", async () => {
            expect(filter(example, 'name , "missing"')).to.deep.equal([example.name, "missing"]);
        });
        it("should concat array , array into array", async () => {
            expect(filter(example, "name , string")).to.deep.equal([example.name, example.string]);
        });
        it("should concat string , array into array", async () => {
            expect(filter(example, '"list" , string')).to.deep.equal(["list", example.string]);
        });
        it("should concat object , string into array", async () => {
            expect(filter(example, 'person , "missing"')).to.deep.equal([
                example.person.name,
                example.person.job,
                "missing"
            ]);
        });
        it("should concat object , object into array", async () => {
            expect(filter(example, "person , complex.0")).to.deep.equal([
                example.person.name,
                example.person.job,
                example.complex[0].name
            ]);
        });
        it("should concat string , object into array", async () => {
            expect(filter(example, '"list" , person')).to.deep.equal(["list", example.person.name, example.person.job]);
        });
    });
    describe("math", () => {
        it("should add strings", async () => {
            expect(filter(example, "name + string")).to.deep.equal("exampletest");
        });
        it("should add numbers", async () => {
            expect(Math.round(10 * filter(example, "number + list.1"))).to.deep.equal(76);
        });
        it("should add numbers", async () => {
            expect(Math.round(10 * filter(example, "number - list.1"))).to.deep.equal(36);
        });
        it("should add numbers", async () => {
            expect(Math.round(10 * filter(example, "list.2 * list.1"))).to.deep.equal(60);
        });
        it("should add numbers", async () => {
            expect(Math.round(10 * filter(example, "list.5 / list.1"))).to.deep.equal(30);
        });
        it("should add numbers", async () => {
            expect(Math.round(10 * filter(example, "number % list.1"))).to.deep.equal(16);
        });
    });
    describe("grouping", () => {
        it("should group", async () => {
            expect(filter(example, "list.1 + (list.2 * list.1)")).to.deep.equal(8);
        });
    });
    describe("functions", () => {
        it("should get data element instead of function if not defined", async () => {
            expect(filter({ $xxx: 6 }, "$xxx")).to.deep.equal(6);
        });
        it("should get data element instead of function if masked", async () => {
            expect(filter({ $length: 6 }, "\\$length")).to.deep.equal(6);
        });
        it("should get length of array", async () => {
            expect(filter(example, "list.$length")).to.deep.equal(6);
        });
        it("should get length of object", async () => {
            expect(filter(example, "$length")).to.deep.equal(10);
        });
        it("should get length of string", async () => {
            expect(filter(example, "name.$length")).to.deep.equal(7);
        });
        it("should use json number", async () => {
            expect(filter(example, "$json(5)")).to.deep.equal(5);
        });
        it("should use json string", async () => {
            expect(filter(example, "$json('\"alex\"')")).to.deep.equal("alex");
        });
        it("should use json list", async () => {
            expect(filter(example, '$json("[1,2,3]")')).to.deep.equal([1, 2, 3]);
        });
        it("should trim string", async () => {
            expect(filter(example, '" test ".$trim')).to.deep.equal("test");
        });
        it("should trim array", async () => {
            expect(filter(example, "list.$trim")).to.deep.equal(example.list);
        });
        it("should trim object", async () => {
            expect(filter(example, "person.$trim")).to.deep.equal(example.person);
        });
        it("should get object keys", async () => {
            expect(filter(example, "person.$keys")).to.deep.equal(["name", "job"]);
        });
        it("should get object keys from list", async () => {
            expect(filter(example, "list.$keys")).to.deep.equal(["0", "1", "2", "3", "4", "5"]);
        });
        it("should clone", async () => {
            expect(filter(example, "list.$clone")).to.deep.equal([1, 2, 3, 4, 5, 6]);
        });
        it("should lowercase", async () => {
            expect(filter(example, "person.name.$lowercase")).to.deep.equal("alexander schilling");
        });
        it("should lowercaseFirst", async () => {
            expect(filter(example, "person.name.$lowercaseFirst")).to.deep.equal("alexander Schilling");
        });
        it("should uppercase", async () => {
            expect(filter(example, "name.$uppercase")).to.deep.equal("EXAMPLE");
        });
        it("should uppercaseFirst", async () => {
            expect(filter(example, "name.$uppercaseFirst")).to.deep.equal("Example");
        });
        it("should check defined", async () => {
            expect(filter(example, "empty.$defined")).to.deep.equal(true);
        });
        it("should check typeof", async () => {
            expect(filter(example, "name.$typeof(string)")).to.deep.equal(true);
        });
        it("should check empty", async () => {
            expect(filter(example, "empty.$empty")).to.deep.equal(true);
        });
        it("should check match", async () => {
            expect(filter(example, "person.name.$match(Alex)")).to.deep.equal(true);
        });
        it("should join", async () => {
            expect(filter(example, "list.$join")).to.deep.equal("1, 2, 3, 4, 5, 6");
        });
        it("should join with separator", async () => {
            expect(filter(example, "list.$join(#)")).to.deep.equal("1#2#3#4#5#6");
        });
        it("should join with multiple separators", async () => {
            const data = [
                ["Alexa", "der"],
                ["Schilli", "g"]
            ];
            expect(filter(data, "$join(' ', n)")).to.deep.equal("Alexander Schilling");
        });
        it("should split with separator", async () => {
            expect(filter(example, "person.name.$split( )")).to.deep.equal(["Alexander", "Schilling"]);
        });
        it("should split with regexp separator", async () => {
            expect(filter(example, "person.name.$split(/[xi]/)")).to.deep.equal(["Ale", "ander Sch", "ll", "ng"]);
        });
        it("should split with multiple separators", async () => {
            expect(filter(example, "person.name.$split(' ', n)")).to.deep.equal([
                ["Alexa", "der"],
                ["Schilli", "g"]
            ]);
        });
        it("should replace using string search", async () => {
            expect(filter(example, "person.name.$replace(l)")).to.deep.equal("Aexander Schilling");
        });
        it("should replace using regexp search", async () => {
            expect(filter(example, "person.name.$replace(/l/g,_)")).to.deep.equal("A_exander Schi__ing");
        });
        it("should left pad string", async () => {
            expect(filter(example, "person.name.$lpad(24)")).to.deep.equal("     Alexander Schilling");
        });
        it("should right pad string", async () => {
            expect(filter(example, "person.name.$rpad(24)")).to.deep.equal("Alexander Schilling     ");
        });
        it("should pad string (oth sides)", async () => {
            expect(filter(example, "person.name.$pad(24)")).to.deep.equal("  Alexander Schilling   ");
        });
        it("should get defined lines", async () => {
            const lines = "line 1\nline 2\nline 3\nline 4\nline 5\nline 6";
            expect(filter(lines, "$line(1,3-5)")).to.deep.equal("line 1\nline 3\nline 4\nline 5");
        });
        it("should compare strings", async () => {
            expect(filter("test", "$compare(=,test)")).to.be.true;
            expect(filter("test1", "$compare(!=,test)")).to.be.true;
        });
        it("should compare numbers", async () => {
            expect(filter(15, "$compare(>,11)")).to.be.true;
            expect(filter(15, "$compare(=,15)")).to.be.true;
        });
        it("should compare dates", async () => {
            expect(filter("2018-06-01", "$compare(>,2018-01-01)")).to.be.true;
            expect(filter("2018-06-30", "$compare(<,2019-01-01)")).to.be.true;
        });
    });
    describe("back references", () => {
        it("should group", async () => {
            expect(filter(example, "list[..]person.name")).to.deep.equal(example.person.name);
            expect(filter(example, "list[..].person.name")).to.deep.equal(example.person.name);
            expect(filter(example, "list.[..].person.name")).to.deep.equal(example.person.name);
        });
    });
    describe("lookup", () => {
        it("should get value at hash", async () => {
            expect(filter(example, "person#name")).to.deep.equal(example.person);
        });
        it("should get value at hash", async () => {
            expect(filter(example, "complex#[]admin")).to.deep.equal([example.complex[1]]);
            expect(filter(example, "complex#*.admin")).to.deep.equal([example.complex[1]]);
        });
    });
});
