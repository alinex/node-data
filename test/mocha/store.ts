import { expect } from 'chai';

import { store } from '../../src/index';

describe('store', () => {
    it('should store in root', function() {
        let data: any = {};
        data = store(data, '', 1);
        expect(data).to.deep.equal(1);
    });
    it('should store in direct element', function() {
        const data: any = {};
        store(data, 'test', 1);
        expect(data?.test).to.deep.equal(1);
    });
    it('should store in sub element', function() {
        const data: any = {};
        store(data, 'test.name', 1);
        expect(data?.test?.name).to.deep.equal(1);
    });
    it('should allow dot in name', function() {
        const data: any = {};
        store(data, 'test\\.name', 1);
        expect(data['test.name']).to.deep.equal(1);
    });
    it('should allow bracket names', function() {
        const data: any = {};
        store(data, '[test.name]', 1);
        expect(data['test.name']).to.deep.equal(1);
    });
});
