import { expect } from 'chai';

import { empty } from '../../src/index';

describe('empty', () => {
    it('should detect undefined', function() {
        expect(empty(undefined)).to.equal(true);
    });
    it('should detect null', function() {
        expect(empty(null)).to.equal(true);
    });
    it('should detect empty array', function() {
        expect(empty([])).to.equal(true);
    });
    it('should detect empty object', function() {
        expect(empty({})).to.equal(true);
    });
    it('should detect empty string', function() {
        expect(empty('')).to.equal(true);
    });
    it('should detect undefined number', function() {
        expect(empty(NaN)).to.equal(true);
    });

    it('should fail on boolean', function() {
        expect(empty(true)).to.equal(false);
    });
    it('should fail on boolean', function() {
        expect(empty(false)).to.equal(false);
    });
    it('should fail on array', function() {
        expect(empty([1, 2])).to.equal(false);
    });
    it('should fail on object', function() {
        expect(empty({ a: 'a' })).to.equal(false);
    });
    it('should fail on string', function() {
        expect(empty('test')).to.equal(false);
    });
    it('should fail on number', function() {
        expect(empty(42)).to.equal(false);
    });
});
