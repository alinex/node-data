import { expect } from 'chai';

import { diff } from '../../src/index';

describe('diff', () => {
    it('should detect addition', function() {
        const a = { a: 1, b: 2 };
        const b = { a: 1, b: 2, c: 3 };
        const d = { c: 'insert' };
        expect(diff(a, b)).to.deep.equal(d);
    });
    it('should detect removal', function() {
        const a = { a: 1, b: 2 };
        const b = { a: 1 };
        const d = { b: 'remove' };
        expect(diff(a, b)).to.deep.equal(d);
    });
    it('should detect change', function() {
        const a = { a: 1, b: 2 };
        const b = { a: 1, b: 3 };
        const d = { b: 'update' };
        expect(diff(a, b)).to.deep.equal(d);
    });
    it('should detect type change', function() {
        const a = { a: 1, b: 2 };
        const b = { a: 1, b: [2] };
        const d = { b: 'update' };
        expect(diff(a, b)).to.deep.equal(d);
    });
    it('should detect deeper change', function() {
        const a = { a: [0, 8, 15], b: 2 };
        const b = { a: [0, 8, 1, 5], b: 2 };
        const d = { 'a.2': 'update', 'a.3': 'insert' };
        expect(diff(a, b)).to.deep.equal(d);
    });
});
