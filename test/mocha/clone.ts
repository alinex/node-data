import { expect } from 'chai';

import { clone } from '../../src/index';

describe('clone', () => {
    it('should clone arrays', function() {
        let base: any[] = [1, 2, 3];
        expect(clone(base)).to.deep.equal(base);
        base = ['alpha', 'beta', 'gamma'];
        expect(clone(base)).to.deep.equal(base);
        base = [{ a: 0 }, { b: 1 }];
        expect(clone(base)).to.deep.equal(base);
        base = [0, 'a', {}, [{}], [function() {}], function() {}];
        expect(clone(base)).to.deep.equal(base);
    });
    it('should deeply clone an array', function() {
        let base = [[{ a: 'b' }], [{ a: 'b' }]];
        let result = clone(base);
        expect(result).to.deep.equal(base);
        expect(result !== base).to.be.true;
        expect(result[0] !== base[0]).to.be.true;
        expect(result[1] !== base[1]).to.be.true;
    });
    it('should deeply clone object', function() {
        const one: any = { a: 'b' };
        const two = clone(one);
        two.c = 'd';
        expect(one).to.not.deep.equal(two);
    });
    it('should deeply clone Map', function() {
        const a = new Map([[1, 5]]);
        const b = clone(a);
        a.set(2, 4);
        expect(Array.from(a)).to.not.deep.equal(Array.from(b));
    });
    it('should deeply clone Set', function() {
        const a = new Set([2, 1, 3]);
        const b = clone(a);
        a.add(8);
        expect(Array.from(a)).to.not.deep.equal(Array.from(b));
    });
    it('should return primitives', function() {
        expect(clone(0)).to.equal(0);
        expect(clone('foo')).to.equal('foo');
    });
    it('should clone a regex', function() {
        expect(clone(/foo/g)).to.deep.equal(/foo/g);
    });
    it('should clone objects', function() {
        expect(clone({ a: 1, b: 2, c: 3 })).to.deep.equal({ a: 1, b: 2, c: 3 });
    });
    it('should deeply clone objects', function() {
        expect(
            clone({ a: { a: 1, b: 2, c: 3 }, b: { a: 1, b: 2, c: 3 }, c: { a: 1, b: 2, c: 3 } })
        ).to.deep.equal({
            a: { a: 1, b: 2, c: 3 },
            b: { a: 1, b: 2, c: 3 },
            c: { a: 1, b: 2, c: 3 }
        });
    });
    it('should deep clone instances with instanceClone true', function() {
        // @ts-ignore
        function A(x: any, y: any, z: any) {
            // @ts-ignore
            this.x = x;
            // @ts-ignore
            this.y = y;
            // @ts-ignore
            this.z = z;
        }
        function B(x: any) {
            // @ts-ignore
            this.x = x;
        }

        // @ts-ignore
        const a = new A({ x: 11, y: 12, z: () => 'z' }, new B(2), 7);
        const b = clone(a, true);
        expect(a).to.deep.equal(b);

        b.y.x = 1;
        b.z = 2;
        expect(a).to.not.deep.equal(b);
        expect(a.z).to.not.equal(b.z);
        expect(a.y.x).to.not.equal(b.y.x);
        // @ts-ignore
    });
    it('should deep clone instances but copy instances', function() {
        // @ts-ignore
        function A(x: any, y: any, z: any) {
            // @ts-ignore
            this.x = x;
            // @ts-ignore
            this.y = y;
            // @ts-ignore
            this.z = z;
        }
        function B(x: any) {
            // @ts-ignore
            this.x = x;
        }

        // @ts-ignore
        const a = new A({ x: 11, y: 12, z: () => 'z' }, new B(2), 7);
        const b = clone(a);
        expect(a).to.deep.equal(b);

        b.y.x = 1;
        b.z = 2;
        expect(a).to.deep.equal(b);
        expect(a.z).to.equal(b.z);
        expect(a.y.x).to.equal(b.y.x);
        // @ts-ignore
    });
});
