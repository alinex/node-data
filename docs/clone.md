title: clone

# Deep Clone

Recursively (deep) clone JavaScript native types, like Object, Array, RegExp, Date as well as primitives.

!!! definition

    Make a deep and complete clone of an object.

    ```ts
    data.clone<T>(source: T, instanceClone: boolean = false): T
    ```
    Parameter:

    - `source` object to be cloned
    - `instanceClone` set to true to also clone instances

    Return:

    - clone of `source` object

This will make a complete clone of the source object. The second parameter defines if instances should be cloned, or copied (the default).

So you may use it like:

```ts
let obj = { a: 'a' };
let arr = [obj];
let copy = data.clone(arr);
```

## Instances

Instances (objects that are from a custom class or are not created by the Object constructor) are not cloned but their reference will be copied by default. To also clone them set the flag in the second parameter to `true`. This works, if the object was created by the `Object` constructor.

{!docs/assets/abbreviations.txt!}
