# Functions

A function or value can be called directly but it often makes more sense to use it behind some filter rule terms. The filter may also go on with the resulting value from the function.

The function call may contain a parameter contained in round rackets behind the name or multiple parameters with `,` as separator. The values can be within single or double quotes, which is needed if a `,` is used within the value.

## String Values

In [combination](combine.md) with the concatenation methods it is sometimes useful to combine a path (described before) with a static value. All values have to be in string representation.
Values can at example be used to add metrics to values. But first we show how it works standalone.

```js
const value = { name: 'example', type: 'a', value: 44 };
console.log(data.filter(value, '"value"'));
// "value"
```

The incoming data is completely ignored and the given string is brought further for filter processing.
This alone is stupid and useless but see the examples below on how to combine it with paths.

See the JSON function below to also inject other data types beside strings.

## JSON Data

To inject any type of data you can use the JSON function. But `$json('"test"')` is the same as the short version for strings `"test"`. On other data types the short form isn't possible.

```js
ds = new DataStore();
console.log(data.filter(value, '$json(\'"value"\')'));
// "value"
console.log(data.filter(value, '$json("[1,2,3]")'));
// [ 1, 2, 3 ]
```

The incoming data is completely ignored and the given string is brought further for filter processing.
Any form of data is possible here and can further processed in the filter rule.

## Manipulation

The following functions are used to change the left hand data.

### Clone

Make a deep clone of this part of the data structure.

```js
const value = [1, 2, 3, 4, 5, 6];
console.log(data.filter(value, '$clone'));
// [1, 2, 3, 4, 5, 6]
```

### Trim

Whitespace at the start or end of the string is cut of. If the input data is not a string it will be converted to one first.

```js
const value = ' test ';
console.log(data.filter(value, '$trim'));
// "trim"
```

If the given data element is an array or object this will be done for each element within the structure. All non string elements will be kept.

### Keys

This will return the keys of the object input.

```js
const value = {
    name: 'Alexander Schilling',
    job: 'Developer'
};
console.log(data.filter(value, '$keys'));
// ['name', 'job']
```

Used on lists this will return the index numbers as list, but as a list of strings:

```js
const value = [1, 2, 3];
console.log(data.filter(value, '$keys'));
// ['0', '1', '2']
```

### Character Case

Strings can be converted using `$lowercase`, `$uppercase`, `$lowercaseFirst` or `$uppercaseFirst`.

```js
const value = 'test';
console.log(data.filter(value, '$uppercase'));
// 'TEST'
```

If the given data element is an array or object this will be done for each element within the structure. All non string elements will be kept.

### Join

Join array elements together into a string:

```js
const value = [1, 2, 3, 4];
console.log(data.filter(value, '$join'));
// '1, 2, 3, 4'
console.log(data.filter(value, '$join(-)'));
// '1-2-3-4'
```

This can also work with two levels, if two separators are given (the last will be used for the inner elements):

```js
const value = [['Alexa', 'der'], ['Schilli', 'g']];
console.log(data.filter(value, '$split(" ", "n")'));
// 'Alexander Schilling'
```

### Split

Split string into array by given phrase or regular expression:

```js
const value = '1,2, 3 ,  4';
console.log(data.filter(value, '$split'));
// [1, 2, 3, 4]
const value = '1-2-3-4';
console.log(data.filter(value, '$split(-)'));
// [1, 2, 3, 4]
const value = 'Alexander Schilling';
console.log(data.filter(value, '$split(/[xi]/)'));
// ['Ale', 'ander Sch', 'll', 'ng']
```

This can also work with two levels, if two separators are given:

```js
const value = 'Alexander Schilling';
console.log(data.filter(value, '$split(" ", "n")'));
// [['Alexa', 'der'],
//  ['Schilli', 'g']]
```

### Replace

A given string may be replaced by another one, also using regular expressions which can also replace multiple occurrences of the same pattern. If no replace string is given, it will be removed:

```js
const value = 'Alexander Schilling';
console.log(data.filter(value, '$replace(l)')); // remove first l
// 'Aexander Schilling'
console.log(data.filter(value, '$replace(/l/g, _)')); // replace all with _
// 'A_exander Schi__ing'
```

### lPad, rPad, Pad

This three methods will pad the string to get at least the defined `length` back. By default spaces are used, but a defined `padding` string may be given as second parameter (repeated if needed). While `lpad` and `rpad` append to any side, `pad` will add on both sides to center the original string (uneven number of padding will add on the right side).

```js
const value = 'Alexander Schilling';
console.log(data.filter(value, '$lpad(25)')); // remove first l
// '     Aexander Schilling'
console.log(data.filter(value, '$rpad(25)')); // remove first l
// 'Aexander Schilling     '
console.log(data.filter(value, '$pad(25)')); // remove first l
// '  Aexander Schilling   '
console.log(data.filter(value, '$lpad(25, .)')); // remove first l
// '.....Aexander Schilling'
```

### Lines

This method will capture only the defined line numbers starting with 1. You can give multiple lines as parameter and also ranges like 5-8:

```js
const lines = 'line 1\nline 2\nline 3\nline 4\nline 5\nline 6';
console.log(data.filter(lines, '$line(1,3-5)'));
// 'line 1\nline 3\nline 4\nline 5'
```

## Test Functions

And the following functions are used to check the data for validity. They are often used in path lookup to decide which elements to keep.

### Defined

Often this is not really necessary to be used because undefined data is always removed in lookup. But to make it more readable the explicit function `$defined` can be used.

### Length

This function will determine the length of the array, the number of keys or the string length.

```js
const value = [1, 2, 3, 4, 5, 6];
console.log(data.filter(value, '$length'));
// 6
ds.data = { name: 'example', type: 'a', value: 44 };
console.log(data.filter(value, '$length'));
// 3
ds.data = 'example';
console.log(data.filter(value, '$length'));
// 7
```

### Type

The following types may be checked using `$typeof(<type>)` with type:

-   `string`
-   `number`
-   `boolean`
-   `object`
-   `array`

```js
const value = 'test';
console.log(data.filter(value, '$typeof(string)'));
// true
```

### Empty

This function will return `true` on empty array, object, string, false or undefined.

```js
const value = [];
console.log(data.filter(value, '$emoty'));
// true
```

### Match

This function will return `true` only if the value matches the given string or regular expression.

```js
const value = 'Alexander Schilling';
console.log(data.filter(value, '$match(Alex)'));
// true
```

### Compare

The value will be compared to a given check value. three types of values can be compared:

- strings with operator: `=`, `!=`
- numbers with operator: `>`, `>=`, `<`, `<=`, `=`, `!=`
- dates with operator: `>`, `>=`, `<`, `<=`, `=`, `!=`

Operator and check value has to be given as parameter for this function:

```js
// strings
data.filter('test', '$compare(=,test)');
data.filter('test1', '$compare(!=,test)');
// numbers
data.filter(15, '$compare(>,11)');
data.filter(15, '$compare(=,15)';
// dates
data.filter('2018-06-01', '$compare(>,2018-01-01)');
data.filter('2018-06-31', '$compare(<,2019-01-01)');
```

If dates are compared localized formats are also parseable if the language is given es third parameter.

{!docs/assets/abbreviations.txt!}
