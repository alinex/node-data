# Combination

You may combine path selection results and direct values together using the following types. This is detected by special characters but also made visible by optional spaces.

## Combine Filters

The `|` operator combines two filters by feeding the output(s) of the one on the left into the input of the one on the right. It’s pretty much the same as the Unix shell’s pipe, if you’re used to that.

If the one on the left produces multiple results, the one on the right will be run for each of those results. So, the expression `[1-3] | foo` retrieves the “foo” field of each element of the input array.

Note that `a.b.c` is the same as `a | b | c`.

```js
const value = {
    car: [
        { type: 'mercedes', color: ['black', 'blue'] },
        { type: 'vw', color: ['silver', 'yellow'] }
    ]
};
console.log(data.filter(value, 'car.0.color.1'));
console.log(data.filter(value, 'car | [0] | color | 1'));
// 'blue'
```

## Alternative

The `||` operator is used to give alternatives. If the left side data selection is not found, the right side selection will be evaluated with the same base as the left side. If this may also be undefined you may add another alternative.

```js
const value = { car: 'mercedes' };
console.log(data.filter(value, 'bike || car'));
// return 'mercedes'
console.log(data.filter(value, 'car || bike'));
// return 'mercedes'
console.log(data.filter(value, 'bike || ship || "boeing"'));
// return 'boeing'
```

## List Concatenation

The `,` operator will create a list containing the left side and right side elements.

If two filters are separated by a comma, then the same input will be fed into both and the two filters’ output value streams will be concatenated.
If on any side there is already an array it's values will be concatenated into the new one. That allows to use this operator to be used multiple times to collect into the same array.

```js
const value = { car: 'mercedes', tire: [1, 2, 3, 4] };
console.log(data.filter(value, '"bike" , "car"'));
// return ['bike', 'car]
console.log(data.filter(value, 'car , "bike"'));
// return ['mercedes', 'bike']
console.log(data.filter(value, 'car, tire'));
// return ['mercedes', 1, 2, 3, 4]
```

If for instance both parts are objects, the values of both will be combined as value list.

```js
const value = {
    garage: { car: 'mercedes', tires: 4 },
    house: { umbrella: 'red' }
};
console.log(data.filter(value, 'garage , house'));
// return ['mercedes', 4, 'red']
```

## Mathematical Operators

In all of the following calculations any value, which is not a number will be treated like 0.

```js
const value = { base: 3 };
console.log(data.filter(value, 'base + 2'));
// return 5
console.log(data.filter(value, 'base - 2"'));
// return 1
console.log(data.filter(value, 'base * 2'));
// return 6
console.log(data.filter(value, '15 / base'));
// return 5
console.log(data.filter(value, '15 % base'));
// return 0
```

## Concatenate Strings

The `+` operator has a second meaning. If used on strings it can concatenate them together.

```js
const value = { name: 'Janina', city: 'London' };
console.log(data.filter(value, 'name + city'));
// return 'JaninaLondon'
console.log(data.filter(value, 'name + " " + city'));
// return 'Janina London'
```

## Grouping

And at last to work in the desired order sometimes braces are necessary. This is best displayed using mathematical operations:

```js
const value = { num: [0, 1, 2, 3, 4, 5, 6] };
console.log(data.filter(value, 'num.2 + (num.3 * num.2)'));
// return 8
```

{!docs/assets/abbreviations.txt!}
