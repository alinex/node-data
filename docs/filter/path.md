title: Path

# Path Selection

A path is used to select specific parts of the data structure.
This can be done in a recursive way. The filter will be resolved step by step and the results will be collected and returned as reference.

## Root Element

The simplest access is to use an empty string which will return the whole data structure.

```js
const value = { name: 'example' };
console.log(data.filter(value, ''));
// { name: 'example' }
```

## Array Element

Array elements can be accessed using the index number (starting with 0). But the position can be also defined using negative Numbers.

```js
const value = ['a', 'b', 'c', 'd', 'e'];
console.log(data.filter(value, '[0]'));
// 'a'
console.log(data.filter(value, '0')); // [] for positive numbers not neccessary
// 'a'
console.log(data.filter(value, '[-3]'));
// 'b'
```

```text
      0    1    2    3    4...
      ↓    ↓    ↓    ↓    ↓
   [ 'a', 'b', 'c', 'd', 'e' ]
      ↑    ↑    ↑    ↑
  ...-4   -3   -2   -1
```

## Multiple Array Elements

Also it is possible to select multiple elements or ranges. This is done by giving the index positions as comma separated list or ranges in which both edge values are inclusive.

```js
const value = ['a', 'b', 'c', 'd', 'e'];
console.log(data.filter(value, '[1,3]'));
// [ 'b', 'd' ]
console.log(data.filter(value, '[1-3]'));
// [ 'b', 'c', 'd' ]
console.log(data.filter(value, '[0-2,4]'));
// [ 'a', 'b', 'c', 'e' ]
console.log(data.filter(value, '[]')); // complete array
// [ 'a', 'b', 'c', 'd', 'e' ]
```

The return value if more than one values are selected will be a list.

A negative selection is also possible by giving an exclamation mark first. This inverts the list meaning all elements not listed are included.

```js
const value = ['a', 'b', 'c', 'd', 'e'];
console.log(data.filter(value, '[!1,3]'));
// [ 'a', 'c', 'd', 'e' ]
console.log(data.filter(value, '[!1-3]'));
// [ 'a', 'd', 'e' ]
console.log(data.filter(value, '[!0-2,4]'));
// [ 'd' ]
```

## Object Element

Within an object, elements will be accessed by their key name.

```js
const value = { name: 'example', value: 44 };
console.log(data.filter(value, 'name'));
// 'example'
console.log(data.filter(value, 'value'));
// 44
```

## Multiple Object Elements

Like in arrays multiple elements can be accessed as list of keys in square brackets.

```js
const value = { name: 'example', type: 'a', value: 44 };
console.log(data.filter(value, '[name,value]'));
// [ 'example', 44 ]
```

But you can also get an object back with the defined keys:

```js
const value = { name: 'example', type: 'a', value: 44 };
console.log(data.filter(value, '{name,value}'));
// { name: 'example', value: 44 }
```

A negative selection possible here, too. The starting exclamation mark inverts the list meaning all elements which keys are not listed are included.

```js
const value = { name: 'example', type: 'a', value: 44 };
console.log(data.filter(value, '{!name,value}'));
// { type: 'a' }
```

## Deep Paths

The above possibilities can also be used recursively in any depth. You can go deeper using the dot like in many languages or the bracket notation:

```js
const value = {
    car: [
        { type: 'mercedes', color: ['black', 'blue'] },
        { type: 'vw', color: ['silver', 'yellow'] }
    ]
};
console.log(data.filter(value, 'car.0.color.1')); // dot notation
console.log(data.filter(value, 'car[0][color][1]')); //bracket notation
// 'blue'
console.log(data.filter(value, 'car[].type')); // wildcard
console.log(data.filter(value, 'car.[].type')); // wildcard
console.log(data.filter(value, 'car.*.type')); // wildcard
// ['mercedes', 'vw']
```

You may also mix bracket- and dot-notation. The bracket notation may also contain spaces and dots in key names, which is not possible in dot-notation.
Alternatively a dot may be masked using `\.` in dot separated paths.

## Backreferences

Sometimes you have filter rules which work from a current position. Therefore set the current position maybe with some backreferences which first will be resolved, before analyzing. Like in file paths it will be resolved and you can also add multiple backreferences.

```js
const value = {
    car: [
        { type: 'mercedes', color: ['black', 'blue'] },
        { type: 'vw', color: ['silver', 'yellow'] }
    ]
};
console.log(data.filter(value, 'car.0.color[..][..].1.type'));
// 'vw'
```

## Lookup

With a `#` marker within the path you define that you will get the element on that position but only if the right side path is fulfilled. This gives you the ability to return all elements which have...

```js
const value = {
    car: [
        { type: 'mercedes', color: ['black', 'blue'], autopilot: true },
        { type: 'vw', color: ['silver', 'yellow'] }
    ]
};
console.log(data.filter(value, 'car.0#autopilot'));
// { type: 'mercedes', color: ['black', 'blue'], autopilot: true }
console.log(data.filter(value, 'car.1#autopilot'));
// undefined
console.log(data.filter(value, 'car[]#autopilot'));
console.log(data.filter(value, 'car.*#autopilot'));
// [{ type: 'mercedes', color: ['black', 'blue'], autopilot: true }]
```

As shown in the last example it can also be used on a list in which case it will filter that list and keep only the elements which has the right side defined.

## RegExp Key Selection

At last within the `[...]` and `{...}` parts you can not only list concrete names but also select them by an regular expression.

```js
const value = {
    os: 'linux',
    proc_num: 5,
    proc_speed: '3.4 MHz'
};
console.log(data.filter(value, '[/^proc_/]'));
// [ 5, '3.4 MHz' ]
console.log(data.filter(value, '{/^proc_/}'));
// { proc_num: 5, proc_speed: '3.4 MHz' }
```

## Special Keys

Keys starting with '$' are possible if no recognized function name follows or if they are masked using '\\$'.
Masking is also possible on the different braces and only need to be set before the first character.

{!docs/assets/abbreviations.txt!}
