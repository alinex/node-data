title: Overview

# Filter

These filter rules can be used to access and transform specific elements of the data structure. The rule is given as text string.

!!! Definition

    Get some parts of the data structure.

    Parameter:

    -   data object structure to work on
    -   command to be used to select elements or data

    Return: elements specified in filter rule command.

    ```ts
    data.filter(data: any, command: string): any
    ```

The filter command is build using the following filter rules:

-   [paths](path.md) which select a specific part of the data
-   [functions](function.md) for more complex tasks
-   [combination](combine.md) of paths and values using different operators

The following pages will show multiple examples how to use this.

{!docs/assets/abbreviations.txt!}
