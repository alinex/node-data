title: diff

# Diff

You can detect which elements within two data structures are different. The result will be a list of all changed parts with type of change (insert, remove, update).

## Diff API

!!! definition

    Find all parts within data structure, which are different.

    Parameter:

    -   `orig` - base structure
    -   `data` - maybe different structure

    Return: map with all (deep) keys which are changed, with values: `insert`, `remove`, `update`

    ```ts
    diff: (orig: any, data: any) => { <path>: 'insert' | 'remove' | 'update' }
    ```

An easy example on a flat data structure will look like:

```ts
import { diff } from '@alinex/data';

const result = diff({ a: 1, b: 2, c: 3 }, { a: 1 , b: 9, d: 4 })
// {
//      b: 'update',
//      c: 'remove',
//      d: 'insert'
// }
```

This will also work with deeper structures and arrays:

```ts
const orig = { a: [0, 8, 15], b: 2 };
const data = { a: [0, 8, 1, 5], b: 2 };
// diff(a, b( = { 'a.2': 'update', 'a.3': 'insert' };
```

{!docs/assets/abbreviations.txt!}
