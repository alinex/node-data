title: merge

# Merge

The merge utility allows to combine multiple data structures together. This is done using a deep merge with:

-   options for object merge
-   options for array merge
-   options for specific elements
-   options specified in data structure
-   optional path specifications
-   clone support
-   source map support

## Merge API

!!! definition

    Marge two or more data structures together.

    Parameter:

    -   `list` specification for each merge input with options:

        - data - real data structures to be merged
        - base - optional base path within the whole setup
        - source - reference to source element or name
        - object - general merge method: combine, replace, missing, alternate (default: combine)
        - array - general merge method: concat, replace, overwrite, combine (default: concat)
        - pathObject - method overwrite for specific paths
        - pathArray - method overwrite for specific paths
        - clone - set to true to make deep clone
        - allowFlags - allow to overwrite in data structure
        - map - generate map showing which result value to it's source

    Return: merged object

    ```ts
    interface MergeInput {
        data: any;
        base?: string;
        source?: any; // default: data
        // default combine methods
        object?:  'combine' | 'replace' | 'missing' | 'alternate'; // default: 'combine'
        array?: 'concat' | 'replace' | 'overwrite' | 'combine'; // default: 'concat'
        // overwrite per position
        pathObject?: {[key: string]: 'combine' | 'replace' | 'missing' | 'alternate' }
        pathArray?: {[key: string]: 'concat' | 'replace' | 'overwrite' | 'combine' }
        // general settings
        clone?: boolean;
        allowFlags?: boolean; // default: true
        map?: boolean; // default: false
    }

    merge: (input: MergeInput, ...) => { data: any, map: any }
    ```

This will give you the result data and also if needed a map in which you can see which value comes from which source.

A short and simple merge example will look like:

```ts
const base = { ... }
const data = { ... }
const combined = merge({ data: base }, { data }).data
```

## Options

As shown in the definition above you could specify a lot of things. Each input data structure is given as an object with the following attributes:

### data

Containing the real data to be merged.

### base

It is possible to add the data to a specified path which is the same as if you add this path as data structure around the values.

`data: 1, base: 'a.b' => { a: { b: 1 } }`

### source

This can be a reference to an object defining the real source of the data. This is only used within the map. If not specified it points to the data structure.

### object

The method, how to combine objects with key/value parts together. This is unnecessary on the first element but will work on all others:

-   combine (default)

    `{a:1, b:2} + {b:4, e:5} => {a:1, b:4, e:5}`

-   replace

    Replace the object completely, if the keys are different like
    `{a:1, b:2} + {b:4, e:5} => {b:4, e:5}`

-   missing

    Add keys which are missing in the previous
    `{a:1, b:2} + {b:4, e:5} => {a:1, b:2, e:5}`

-   alternate

    Will only use the later value if the first is empty `{}`
    `{a:1, b:2} + {b:4, e:5} => {a:1, b:2}` but
    `{} + {b:4, e:5} => {b:4, e:5}`

### array

The method, how to combine array objects together. This is unnecessary on the first element but will work on all others:

-   concat (default)

    This will add the second array behind the first one like
    `[ 1, 2, 3 ] + [ 4, 5] => [ 1, 2, 3, 4, 5 ]`

-   replace

    Replaces the existing array completely like
    `[ 1, 2, 3 ] + [ 4, 5] => [ 4, 5 ]`

-   overwrite

    Instead of completely replacing it, the specified index positions will overwrite each other
    `[ 1, 2, 3 ] + [ 4, 5] => [ 4, 5, 3 ]`

-   combine

    This will go into each array element and merge the containing subelements
    `[ {a:1}, {b:2} ] + [ {d:4} ] => [ {a:1, d:4}, {b:2} ]`

### pathObject, pathArray

Specific merge setting for only the directly specified path.

### clone

The source data structures will be kept unchanged but this takes a bit more memory and time. So if you no longer need the source structures set this to`false`.

### allowFlags

By default it is possible to change the preset merge options for a single merge at a specific position by adding a flag value within the data. If this should be disallowed set it to `false`. Like object and array combination methods they only are effective from the second input ongoing and have to be set there.

### map

By default mapping is switched of because it needs more memory and performance so only switch it on if you need it. Often this is used for debugging. If not switched on the map will be an empty object.

## Algorithm Specification

Different type of objects will always replace, but for equal objects (array, object) the specified algorithm is used in the following order:

1. setting within source data (to be merged), needs `allowFlags = true`
2. specification for the defined path in `pathObject` or `pathArray`
3. general specification in `object` or `array`
4. default settings

### Settings in data

Flags within the data structure have to be a string as **last** array **element**. It will be used and the element will be included in merge:

```ts
[1, 2, 3, 'MERGE_ARRAY_REPLACE'];
```

Or an object setting under key `MERGE_OBJECT`, which may be at any position. It will be used and the element will be included in merge:

```ts
{ MERGE_OBJECT: 'alternate', name: 'Unknown' }
```

Have a look at the examples below to get a better understanding.

## Examples

The following examples will not show everything but a lot of the possibilities on some data structures complete with the map support.

=== "data_x"

    ```ts
    const data_x = {
        name: 'Unknown',
        hair: {
            color: 'brown'
        },
        family: ['mom', 'dad']
    };
    ```

=== "data_y"

    ```ts
    const data_y = {
        name: 'Mark',
        gender: 'Male',
        hair: {
            cut: 'short'
        },
        family: ['wife']
    };
    ```

=== "code"

    ```ts
    const result = merge(
        {
            data: data_x,
            source: 'x',
            map: true
        },
        {
            data: data_y
            source: 'y',
            map: true
        }
    );
    ```

=== "result"

    ```ts
    {
        name: 'Mark',
        gender: 'Male',
        hair: {
            color: 'brown',
            cut: 'short'
        },
        family: ['mom', 'dad', 'wife']
    }
    ```

=== "map"

    ```ts
    {
        __map: ['x', ''],
        name: { __map: ['y', 'name'] },
        hair: {
            __map: ['x', 'hair'],
            color: { __map: ['x', 'hair.color'] },
            cut: { __map: ['y', 'hair.cut'] }
        },
        family: {
            '0': { __map: ['x', 'family.0'] },
            '1': { __map: ['x', 'family.1'] },
            '2': { __map: ['y', 'family.0'] },
            __map: ['x', 'family']
        },
        gender: { __map: ['y', 'gender'] }
    }
    ```

### Base Path

Move data structure to be positioned under base path.

```ts
// data: 1, base: 'a.b' => { a: { b: 1 } }
const result = merge({ data: 1, base: 'a.b', source: 'x', map: true });
expect(result.data).to.deep.equal({ a: { b: 1 } });
expect(result.map).to.deep.equal({
    a: { b: { __map: ['x', 'a.b'] }, __map: ['x', 'a'] },
    __map: ['x', '']
});
```

### Object methods

Combine:

```ts
// {a:1, b:2} + {b:4, e:5} => {a:1, b:4, e:5}
const result = merge(
    { data: { a: 1, b: 2 }, source: 'x', map: true },
    { data: { b: 4, e: 5 }, source: 'y', object: `combine`, map: true }
);
expect(result.data).to.deep.equal({ a: 1, b: 4, e: 5 });
expect(result.map).to.deep.equal({
    a: { __map: ['x', 'a'] },
    b: { __map: ['y', 'b'] },
    e: { __map: ['y', 'e'] },
    __map: ['x', '']
});
```

Replace:

```ts
// {a:1, b:2} + {b:4, e:5} => {b:4, e:5}
const result = merge(
    { data: { a: 1, b: 2 }, source: 'x', map: true },
    { data: { b: 4, e: 5 }, source: 'y', object: `replace`, map: true }
);
expect(result.data).to.deep.equal({ b: 4, e: 5 });
expect(result.map).to.deep.equal({
    b: { __map: ['y', 'b'] },
    e: { __map: ['y', 'e'] },
    __map: ['y', '']
});
```

Missing:

```ts
// {a:1, b:2} + {b:4, e:5} => {a:1, b:2, e:5}
const result = merge(
    { data: { a: 1, b: 2 }, source: 'x', map: true },
    { data: { b: 4, e: 5 }, source: 'y', object: `missing`, map: true }
);
expect(result.data).to.deep.equal({ a: 1, b: 2, e: 5 });
expect(result.map).to.deep.equal({
    a: { __map: ['x', 'a'] },
    b: { __map: ['x', 'b'] },
    e: { __map: ['y', 'e'] },
    __map: ['x', '']
});
```

Alternate:

```ts
// {a:1, b:2} + {b:4, e:5} => {a:1, b:2}
const result = merge(
    { data: { a: 1, b: 2 }, source: 'x', map: true },
    { data: { b: 4, e: 5 }, source: 'y', object: `alternate`, map: true }
);
expect(result.data).to.deep.equal({ a: 1, b: 2 });
expect(result.map).to.deep.equal({
    a: { __map: ['x', 'a'] },
    b: { __map: ['x', 'b'] },
    __map: ['x', '']
});
```

```ts
// {} + {b:4, e:5} => {b:4, e:5}
const result = merge(
    { data: {}, source: 'x', map: true },
    { data: { b: 4, e: 5 }, source: 'y', object: `alternate`, map: true }
);
expect(result.data).to.deep.equal({ b: 4, e: 5 });
expect(result.map).to.deep.equal({
    b: { __map: ['y', 'b'] },
    e: { __map: ['y', 'e'] },
    __map: ['y', '']
});
```

### Array methods

Concat:

```ts
// [ 1, 2, 3 ] + [ 4, 5] => [ 1, 2, 3, 4, 5 ]
const result = merge(
    { data: [1, 2, 3], source: 'x', map: true },
    { data: [4, 5], source: 'y', array: `concat`, map: true }
);
expect(result.data).to.deep.equal([1, 2, 3, 4, 5]);
expect(result.map).to.deep.equal({
    0: { __map: ['x', '0'] },
    1: { __map: ['x', '1'] },
    2: { __map: ['x', '2'] },
    3: { __map: ['y', '0'] },
    4: { __map: ['y', '1'] },
    __map: ['x', '']
});
```

Replace:

```ts
// [ 1, 2, 3 ] + [ 4, 5] => [ 4, 5 ]
const result = merge(
    { data: [1, 2, 3], source: 'x', map: true },
    { data: [4, 5], source: 'y', array: `replace`, map: true }
);
expect(result.data).to.deep.equal([4, 5]);
expect(result.map).to.deep.equal({
    0: { __map: ['y', '0'] },
    1: { __map: ['y', '1'] },
    __map: ['y', '']
});
```

Overwrite:

```ts
// [ 1, 2, 3 ] + [ 4, 5] => [ 4, 5, 3 ]
const result = merge(
    { data: [1, 2, 3], source: 'x', map: true },
    { data: [4, 5], source: 'y', array: `overwrite`, map: true }
);
expect(result.data).to.deep.equal([4, 5, 3]);
expect(result.map).to.deep.equal({
    0: { __map: ['y', '0'] },
    1: { __map: ['y', '1'] },
    2: { __map: ['x', '2'] },
    __map: ['x', '']
});
```

Combine:

```ts
// [ {a:1}, {b:2} ] + [ {d:4} ] => [ {a:1, d:4}, {b:2} ]
const result = merge(
    { data: [{ a: 1 }, { b: 2 }], source: 'x', map: true },
    { data: [{ d: 4 }], source: 'y', array: `combine`, map: true }
);
expect(result.data).to.deep.equal([{ a: 1, d: 4 }, { b: 2 }]);
expect(result.map).to.deep.equal({
    0: {
        a: { __map: ['x', '0.a'] },
        d: { __map: ['y', '0.d'] },
        __map: ['x', '0']
    },
    1: { b: { __map: ['x', '1.b'] }, __map: ['x', '1'] },
    __map: ['x', '']
});
```

### Specific method

Use a method only for a specific path in the structure.

```ts
// { normal: [ 1, 2, 3 ], specific: [ 1, 2, 3 ] }
// + { normal: [ 4, 5], specific: [ 4, 5] }
// => { normal: [ 1, 2, 3, 4, 5 ], specific: [ 4, 5 ] }
const result = merge(
    { data: { normal: [1, 2, 3], specific: [1, 2, 3] }, source: 'x', map: true },
    {
        data: { normal: [4, 5], specific: [4, 5] },
        source: 'y',
        pathArray: { specific: 'replace' },
        map: true
    }
);
expect(result.data).to.deep.equal({ normal: [1, 2, 3, 4, 5], specific: [4, 5] });
expect(result.map).to.deep.equal({
    __map: ['x', ''],
    normal: {
        '0': { __map: ['x', 'normal.0'] },
        '1': { __map: ['x', 'normal.1'] },
        '2': { __map: ['x', 'normal.2'] },
        '3': { __map: ['y', 'normal.0'] },
        '4': { __map: ['y', 'normal.1'] },
        __map: ['x', 'normal']
    },
    specific: {
        '0': { __map: ['y', 'specific.0'] },
        '1': { __map: ['y', 'specific.1'] },
        __map: ['y', 'specific']
    }
});
```

### Method flags in data

```ts
// { normal: [ 1, 2, 3 ], specific: [ 1, 2, 3 ] }
// + { normal: [ 4, 5], specific: [ 4, 5] }
// => { normal: [ 1, 2, 3, 4, 5 ], specific: [ 4, 5 ] }
const result = merge(
    { data: { normal: [1, 2, 3], specific: [1, 2, 3] }, source: 'x', map: true },
    {
        data: { normal: [4, 5], specific: [4, 5, 'MERGE_ARRAY_REPLACE'] },
        source: 'y',
        map: true
    }
);
expect(result.data).to.deep.equal({ normal: [1, 2, 3, 4, 5], specific: [4, 5] });
expect(result.map).to.deep.equal({
    __map: ['x', ''],
    normal: {
        '0': { __map: ['x', 'normal.0'] },
        '1': { __map: ['x', 'normal.1'] },
        '2': { __map: ['x', 'normal.2'] },
        '3': { __map: ['y', 'normal.0'] },
        '4': { __map: ['y', 'normal.1'] },
        __map: ['x', 'normal']
    },
    specific: {
        '0': { __map: ['y', 'specific.0'] },
        '1': { __map: ['y', 'specific.1'] },
        __map: ['y', 'specific']
    }
});
```

{!docs/assets/abbreviations.txt!}
