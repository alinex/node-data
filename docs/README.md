title: Overview

# Alinex Data Helper

![core icon](https://assets.gitlab-static.net/uploads/-/system/project/avatar/13521613/structure.png){: .right .icon}

This is a collection of methods to work on JavaScript data structures which are a combination of objects, array and other data types.

The following methods are available:

-   [clone](clone.md) - make a exact copy of the complete data structure
-   [filter](filter) - access and transform specific elements
-   [store](store) - set element at defined path
-   [merge](merge.md) - bring two or more structures together using different merge methods
-   [diff](diff.md) - returns all differences between two data structures
-   [equal](equal.md) - check if two data structures are deeply equal
-   [empty](empty.md) - check if data structure or value is empty

Together this methods are a complete suite of helpers to manage the data structure in memory.
Use [DataStore](https://alinex.gitlab.io/node-datastore) to export or import them or [Validator](https://alinex.gitlab.io/node-validator) to also check them against a defined schema.

## Installation

To include in your module install it using:

```bash
npm install @alinex/data --save
```

Now only load it in your code and call it's methods.

## Usage

Load the methods you need:

=== "TypeScript"

    ```ts
    // load default function (only filter)
    import data from '@alinex/data';
    // load all functions
    import * as data from '@alinex/data';
    // load specific function into own variables
    import { clone } from '@alinex/data';
    ```

=== "JavaScript"

    ```js
    // load only the default functions (only filter)
    const data = require('@alinex/data').default;
    // load all functions
    const data = require('@alinex/data');
    ```

Only the `filter` method is exported by default.

### filter

To get a specific element by path:

```ts
import { filter } from '@alinex/data';
const path = 'some.path[to].value';
const struct = ...
const result = filter(struct, path);
```

See more under [filter](filter).

### clone

To clone a object to prevent changes be made on the origin one:

```ts
import { clone } from '@alinex/data';
const data = ...
const copy = clone(data);
```

```ts
import * as data from '@alinex/data';
const data = ...
const copy = data.clone(data);
```

See more under [clone](clone).

### merge

This will bring two or more data structures together. The methods can be changed.

```ts
import { merge } from '@alinex/data';

const result = merge(
    {
        data: data_x,
        source: 'x',
        map: true
    },
    {
        data: data_y
        source: 'y',
        map: true
    }
);
```

See more under [merge](merge).

### diff

You can detect which elements within two data structures are different:

```ts
import { diff } from '@alinex/data';

const result = diff({ a: 1, b: 2, c: 3 }, { a: 1, b: 9, d: 4 });
// {
//      b: 'update',
//      c: 'remove',
//      d: 'insert'
// }
```

This will also work with deeper structures and arrays.

### equal

This will check if two data structures are completely identical.

```ts
import { equal } from '@alinex/data';

const result = equal(
    { data: [1 2, 3] },
    { data: [1 2, 3] }
);
// true
```

### empty

Check if the give value or data structure is empty.

```ts
import { empty } from '@alinex/data';

const result = empty({});
// true
```

## Debugging

Like in all my modules the [debug](https://www.npmjs.com/package/debug) module is included. See the also the [my documentation](https://alinex.gitlab.io/node-core/standard/debug/) on how to use and control it.

The data module uses the following names:

-   `data:*` - to get all
-   `data:clone` - show data structures to be cloned
-   `data:filter` - output each initial filter call with options and some steps within the processing
-   `data:merge` - display target, source, options, map for each merge step and final result

## Support

I don't give any paid support but you may create [GitLab Issues](https://gitlab.com/alinex/node-data/-/issues):

-   Bug Reports or Feature: Please make sure to give as much information as possible. And explain how the system should behave in your opinion.
-   Code Fixing or Extending: If you help to develop this package I am thankful. Develop in a fork and make a merge request after done. I will have a look at it.
-   Should anybody be willing to join the core team feel free to ask, too.
-   Any other comment or discussion as far as it is on this package is also kindly accepted.

Please use for all of them the [GitLab Issues](https://gitlab.com/alinex/node-data/-/issues) which only requires you to register with a free account.

{!docs/assets/stats-pdf-license.txt!}

{!docs/assets/abbreviations.txt!}
