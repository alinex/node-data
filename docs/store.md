title: store

# Store

Store value at defined path within data structure. Already set elements will be overwritten while not existing paths will be created.

## Store API

!!! definition

    Store value at defined path in data structure.

    Parameter:

    - `data` - object structure to work on
    - `path` - to store valuie at
    - `value` - to be set

    Return: `data` structure which is the same as given as long as the root elment wasn't changed

    ```ts
    store: (data: any, path: string, value: any) => data: any
    ```

The path may be:

- '' - for the root element
- 'name' - set the name element
- 'address.name' - set name under address (create address if not existing)
- 'log\\.txt' - don't treat as path, dot is part of the name
- '[log.txt]' - alternative syntax to the above
- 'file[log.txt].size' - both types mixed

{!docs/assets/abbreviations.txt!}
