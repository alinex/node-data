# Last Changes

## Version 1.5.0 (08.05.2021)

-   upgrade to ES2019, Node >= 12
-   upgrade chrono date parsing
   
-   Hotfix 1.5.1 (09.05.2021) - fix typescript settings

## Version 1.4.0 (09.11.2019)

-   add `store()` method to set value at defined paths
-   allow `\.` to be used in filter to mask dot

-   Update 1.4.1 (15.02.2020) - update package management
-   Update 1.4.2 (15.02.2020) - merging with origin
-   Update 1.4.3 (10.09.2020) - update packages
-   Update 1.4.4 (23.10.2020) - update packages and doc structure
-   Update 1.4.5 (02.01.2021) - updated doc theme and packages

## Version 1.3.0 (02.11.2019)

-   add function `compare(operator, check)` to compare string, number and dates
-   add `diff()` method to detect changes between two structures

## Version 1.2.0 (01.09.2019)

Extension:

-   multiple: `$split(row, col)` and `$join(row, col)` -> something like: \n, \t
-   add function `$replace(a,b)` supporting string or regexp search
-   add function `$lpad(num, padding)`, `$rpad(num, padding)`, `$pad(num, padding)`
-   add function `$line(1,3-4)` to only go on with this lines
-   add function `$match(re)` to check if string or regular expression matches

Version 1.2.1 (16.09.2019)

-   filter: allow '_' in paths like '[]' i.e. address._.name
-   filter: allow access paths to start with '$' if no function or with '\$...' to prevent function call and use as element
-   Bug Fix: `$lowercaseFirst` not callable because internally not working case sensitivity

Version 1.2.2 (01.10.2019)

-   Bug Fix: Accessing elements below `$` like `status.$.code`

Version 1.2.3 (08.10.2019)

-   Bug Fix: Merging without mapping is broken in object combine

## Version 1.1.0 (11.08.2019)

Extension:

-   add `equal()` and `empty()` method for deep equal check
-   more settings for MergeInput:
-   depend: sources - only use if at least one of the others is set
-   alternate: sources - only use if none of the others are present
-   new filter functions: join, split

Fixes:

-   optimized debugging
-   fix bug in example docs

## Version 1.0.0 (01.08.2019)

Initial version extracted from [DataStore](https://alinex.gitlab.io/node-datastore) containing:

-   clone - to deep cloning any data structure
-   filter - access and transform specific elements
-   merge - bring two or more structures together using different merge methods

Hotfix 1.0.1 (04.08.2019) allow to give MergeInput without data.

{!docs/assets/abbreviations.txt!}
