title: equal

# Deep Equal

Check if two data structures are completely equal to each other.

!!! definition

    ```ts
    data.equal(a: any, b:any): boolean
    ```

    Parameter:

    - `c` First data structure.
    - `b` Second data structure.

    Return:

    - `true` if equal, else `false`

{!docs/assets/abbreviations.txt!}
