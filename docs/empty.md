title: empty

# Deep Empty

Check if the value or data structure is empty.

!!! definition

    ```ts
    data.empty(value: any): boolean
    ```

    Parameter:

    - `value` value or data structure to check

    Return:

    - `true` if empty, else `false`

{!docs/assets/abbreviations.txt!}
