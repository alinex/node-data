import { clone } from './clone';
import { filter } from './filter';
import { store } from './store';
import { diff } from './diff';
import { merge } from './merge';
import { equal } from './equal';
import { empty } from './empty';

export { filter, store, clone, merge, diff, equal, empty };
export default { filter };
