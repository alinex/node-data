// import Debug from 'debug';

// const debug = Debug('data:empty');

/**
 * Check if the given value is empty or an empty object.
 * @param value To be checked as if it is empty.
 */
export function empty(value: any): boolean {
    if (value == null) return true;
    if ('boolean' == typeof value) return false;
    if ('number' == typeof value) return isNaN(value);
    if ('string' == typeof value) return value.length === 0;
    if ('function' == typeof value) return value.length === 0;
    if (Array.isArray(value)) return value.length === 0;
    if ('object' == typeof value) {
        if ('number' == typeof value.size) return value.size === 0;
        return Object.keys(value).length === 0;
    }
    return false;
}

export default empty;
