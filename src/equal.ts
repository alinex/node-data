// import Debug from 'debug';
import { default as deepequal } from 'fast-deep-equal';

// const debug = Debug('data:equal');

/**
 * Check if both datastructures are deep equal.
 * @param c First data structure.
 * @param b Second data structure.
 */
export function equal(c: any, b: any): boolean {
    return deepequal(c, b);
}

export default equal;
