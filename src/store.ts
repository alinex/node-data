/**
 * Store value at defined path in data structure.
 * @param data object structure to work on
 * @param path to store valuie at
 * @param value to be set
 */
export function store(data: any, path: string, value: any): any {
    if (path === '') return value;
    // access named element
    let m = path.match(/^(\[(.*?)\]|([^[]+?))((?<!\\)\.(.*)|(\[.*))?$/);
    if (m) {
        const key = (m[2] || m[1]).replace(/\\\./g, '.');
        const sub = m[6] || m[5];
        if (sub) {
            if (!data[key]) data[key] = {};
            data[key] = store(data[key], sub, value);
        } else data[key] = value;
    }
    return data;
}
