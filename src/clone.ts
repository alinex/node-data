import Debug from 'debug';

const deepclone = require('clone-deep');

const debug = Debug('data:clone');

/**
 * Make a deep and complete clone of an object.
 * @param source object to be cloned
 * @param instanceClone set to true to also clone instances
 */
export function clone<T>(source: T, instanceClone: boolean = false): T {
    debug('%O', source);
    return deepclone(source, instanceClone);
}

export default clone;
