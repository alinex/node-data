// -   diff between two structures
//     -   add - in b but not in a
//     -   remove - in a but not in b
//     -   change - not equal or any diff type below

export function diff(orig: any, data: any): any {
    const changes: any = {};
    const aList = Object.keys(orig);
    const bList = Object.keys(data);
    aList.forEach(k => {
        if (!bList.includes(k)) changes[k] = 'remove';
        else if (typeof orig[k] !== typeof data[k]) changes[k] = 'update';
        else if (Array.isArray(orig[k]) !== Array.isArray(data[k])) changes[k] = 'update';
        else if (typeof orig[k] === 'object') {
            const sub = diff(orig[k], data[k]);
            Object.keys(sub).forEach(e => {
                changes[`${k}.${e}`] = sub[e];
            });
        } else if (orig[k] !== data[k]) changes[k] = 'update';
    });
    bList.forEach(k => {
        if (!aList.includes(k)) changes[k] = 'insert';
    });
    return changes;
}
