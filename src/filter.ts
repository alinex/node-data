import Debug from "debug";
// @ts-ignore
import * as chrono from "chrono-node";

import { clone, empty } from "./index";

const debug = Debug("data:filter");

/**
 * Get some parts of the data structure.
 * @param data object structure to work on
 * @param command to be used to select elements or data
 */
export function filter(data: any, command: string): any {
    debug(`filter %o on %O`, command, data);
    command = command.trim();
    // replace back references [..]
    let old: string;
    do {
        old = command;
        command = command
            .replace(/(?:\.[^\[\]\{\}]+\.?|\[(.*?)\]|\{(.*?)\})\.?\[\.\.\]/, "")
            .replace(/(?:[^\[\]\{\}]+\.?|\[(.*?)\]|\{(.*?)\})\.?\[\.\.\]/, "")
            .replace(/^\./, "");
    } while (old != command);
    // resolve
    const res = partResolve(data, command);
    debug(`result %O`, res);
    return res;
}

function partResolve(data: any, command: string): any {
    const end = posEndPart(command);
    const braces = command[0] === "(";
    // only path or value
    if (!braces && end === command.length) return pathValue(data, command);
    // combinations
    const base = braces ? partResolve(data, command.substring(1, end - 1)) : pathValue(data, command.substring(0, end));
    return combine(data, base, command.substring(end).trim());
}

function combine(data: any, base: any, command: string): any {
    let res: any;
    let m = command.match(/^([^\ ]*)/);
    const operator = m ? m[1] : "|";
    debug("part combine using %o", operator);
    command = command.substring(operator.length).trim();
    if (command.length === 0) throw new Error(`Missing operand in data filter expression after: ${operator}`);
    const end = posEndPart(command);
    const braces = command[0] === "(";
    // run result operators
    let add: any;
    // resolve for calculating operaand
    if (!["|"].includes(operator))
        add = braces ? partResolve(data, command.substring(1, end - 1)) : pathValue(data, command.substring(0, end));
    let num: number;
    switch (operator) {
        case "|": // pipe
            res = braces
                ? partResolve(base, command.substring(1, end - 1))
                : pathValue(base, command.substring(0, end));
            break;
        case "||": // alternative
            if (base) res = base;
            else res = add;
            break;
        case ",":
            res = [];
            if (Array.isArray(base)) res = res.concat(base);
            else if (typeof base === "object") res = res.concat(Object.values(base));
            else res.push(base);
            if (Array.isArray(add)) res = res.concat(add);
            else if (typeof add === "object") res = res.concat(Object.values(add));
            else res.push(add);
            break;
        case "+":
            num = Number.parseFloat(base);
            if (isNaN(num) || isNaN(Number.parseFloat(add))) {
                // string
                res = base.toString() + add.toString();
            } else {
                // math
                res = isNaN(num) ? 0 : num;
                num = Number.parseFloat(add);
                if (!isNaN(num)) res += num;
            }
            break;
        case "-":
            num = Number.parseFloat(base);
            res = isNaN(num) ? 0 : num;
            num = Number.parseFloat(add);
            if (!isNaN(num)) res -= num;
            break;
        case "*":
            num = Number.parseFloat(base);
            res = isNaN(num) ? 0 : num;
            num = Number.parseFloat(add);
            if (!isNaN(num)) res *= num;
            break;
        case "/":
            num = Number.parseFloat(base);
            res = isNaN(num) ? 0 : num;
            num = Number.parseFloat(add);
            if (!isNaN(num) && num != 0) res /= num;
            break;
        case "%":
            res = 0;
            num = Number.parseFloat(base);
            if (!isNaN(num)) res = num;
            num = Number.parseFloat(add);
            if (!isNaN(num)) res %= num;
            break;
        default:
            throw new Error(`Unknown operator in data access filter: ${operator}`);
    }
    // done
    if (end === command.length) return res;
    // more operators
    return combine(data, res, command.substring(end).trim());
}

export function posEndPart(command: string): number {
    // find the first space as end of the current term
    // but respect all kinds of braces and double quotes
    // escaping quotes are possible
    let level = 0;
    let quote = false;
    const max = command.length;
    for (let pos = 0; pos < max;) {
        const char = command[pos++];
        if (char === '"' && command[pos - 2] !== "\\") quote = !quote;
        else if (quote) continue;
        else if (["(", "[", "{"].includes(char)) level++;
        else if ([")", "]", "}"].includes(char)) level--;
        else if (level === 0 && char === " ") return pos - 1;
    }
    return max;
}

export function posEndFunction(command: string): number {
    // find the first space as end of the current term
    // but respect all kinds of braces and double quotes
    // escaping quotes are possible
    let level = 0;
    let quote = false;
    const max = command.length;
    for (let pos = 1; pos < max;) {
        const char = command[pos++];
        if (char === '"' && command[pos - 2] !== "\\") quote = !quote;
        else if (quote) continue;
        else if (char === "(") level++;
        else if (char === ")") {
            level--;
            if (level === 0) return pos;
        } else if (level === 0 && [".", "[", "{"].includes(char)) return pos - 1;
    }
    return max;
}

export function pathValue(data: any, command: string): any {
    debug("read value at path %o", command);
    // root access
    if (command === "") return data;
    // value
    let m = command.match(/^"((?:[^"\\]|\\.)*)"\.?(.*)/);
    if (m) return pathValue(m[1].replace(/\\(.)/g, "$1"), m[2]);
    // function
    m = command.match(/^\$[^.]/);
    if (m) {
        const end = posEndFunction(command);
        return getFnResult(data, command.substring(1, end), command.substring(end));
    }
    if (!data) return data;
    // lookup
    if (command.substr(0, 1) === "#") {
        if (Array.isArray(data)) return data.filter(e => pathValue(e, command.substr(1)));
        if (pathValue(data, command.substr(1))) return data;
        return undefined;
    }
    // wildcard
    m = command.match(/^\*(\.(.*)|([#\{\[].*))?/);
    if (m) return getElementList(data, "", m[3] || m[2]);
    // access named element
    m = command.match(/^([^\[\{}#]+?)((?<!\\)\.(.*)|([#\{\[].*))/);
    if (m) return getNamedElement(data, m[1], m[4] || m[3]);
    // array access [...]
    m = command.match(/^\[(.*?)\]\.?(.*)/);
    if (m) return getElementList(data, m[1], m[2]);
    // object parts {...}
    m = command.match(/^\{(.*?)\}\.?(.*)/);
    if (m) return getElementMap(data, m[1], m[2]);
    // direct element
    return getNamedElement(data, command.replace(/^\\([^\\])/, "$1"), "");
}

function getFnResult(data: any, call: string, command: string): any {
    let res: any;
    const m = call.match(/^([^(]*?)(?:\((.*?)\))?$/);
    if (!m) throw new Error("Missing function name after '$'");
    const param = m[2]
        ? m[2].split(/ *, *(?=(?:(?:[^"]*"){2})*[^"]*$)/).map(e => e.replace(/^(['"])(.*)\1$/, "$2"))
        : [];
    if (!m[1] || !fn[m[1]]) {
        debug("Function $%s not defined in datastore filters -> direct element access.", m[1]);
        return getNamedElement(data, `$${call}${command}`, "");
    }
    debug("call function %o with parameters %o", m[1], param);
    res = fn[m[1]](data, ...param);
    return command ? pathValue(res, command) : res;
}

const fn: { [key: string]: Function } = {
    json: (data: any, opt: string): any => {
        return JSON.parse(opt);
    },
    length: (data: any): number | undefined => {
        if (!data) return 0;
        if (Array.isArray(data)) return data.length;
        if (typeof data === "object") return Object.keys(data).length;
        return ("" + data).length;
    },
    keys: (data: any): any[] | undefined => {
        if (typeof data === "undefined") return undefined;
        if (typeof data === "object") return Object.keys(data);
        return [];
    },
    trim: (data: any): any => {
        if (!["string", "object"].includes(typeof data)) return data;
        if (Array.isArray(data)) return data.map(e => fn.trim(e));
        if (typeof data === "object") {
            const res: any = {};
            Object.keys(data).map(k => (res[k] = fn.trim(data[k])));
            return res;
        }
        return ("" + data).trim();
    },
    clone: (data: any): any => clone(data),
    lowercase: (data: any): any => {
        if (!["string", "object"].includes(typeof data)) return data;
        if (Array.isArray(data)) return data.map(e => fn.lowercase(e));
        if (typeof data === "object") {
            const res: any = {};
            Object.keys(data).map(k => (res[k] = fn.lowercase(data[k])));
            return res;
        }
        return ("" + data).toLowerCase();
    },
    lowercaseFirst: (data: any): any => {
        if (!["string", "object"].includes(typeof data)) return data;
        if (Array.isArray(data)) return data.map(e => fn.lowercaseFirst(e));
        if (typeof data === "object") {
            const res: any = {};
            Object.keys(data).map(k => (res[k] = fn.lowercaseFirst(data[k])));
            return res;
        }
        const text = "" + data;
        return text.substr(0, 1).toLowerCase() + text.substr(1);
    },
    uppercase: (data: any): any => {
        if (!["string", "object"].includes(typeof data)) return data;
        if (Array.isArray(data)) return data.map(e => fn.uppercase(e));
        if (typeof data === "object") {
            const res: any = {};
            Object.keys(data).map(k => (res[k] = fn.uppercase(data[k])));
            return res;
        }
        return ("" + data).toUpperCase();
    },
    uppercaseFirst: (data: any): any => {
        if (!["string", "object"].includes(typeof data)) return data;
        if (Array.isArray(data)) return data.map(e => fn.uppercaseFirst(e));
        if (typeof data === "object") {
            const res: any = {};
            Object.keys(data).map(k => (res[k] = fn.uppercaseFirst(data[k])));
            return res;
        }
        const text = "" + data;
        return text.substr(0, 1).toUpperCase() + text.substr(1);
    },
    defined: (data: any): boolean => {
        return typeof data !== "undefined";
    },
    typeof: (data: any, type: string): boolean => {
        if (type === "array") return Array.isArray(data);
        return typeof data === type;
    },
    empty: (data: any): boolean => empty(data),
    match: (data: any, search: string): boolean => {
        if (typeof data !== "string") return false;
        let m = search.match(/^\/(.*)\/([gimy])*$/);
        let s = m ? RegExp(m[1], m[2]) : search;
        return data.match(s) ? true : false;
    },
    join: (data: any, ...separator: string[]): string => {
        if (!Array.isArray(data)) return data;
        if (!separator.length) separator = [", "];
        // level 2
        data = data.map(e => (Array.isArray(e) ? e.join(separator[1] || separator[0]) : e));
        // level 1
        return data.join(separator[0]);
    },
    split: (data: any, ...separator: string[]): string[] => {
        if (typeof data !== "string") return data;
        if (!separator.length) separator = ["/ *, */"];
        // level 1
        let m = separator[0].match(/^\/(.*)\/$/);
        let s = m ? RegExp(m[1]) : separator[0];
        data = data.split(s);
        // level 2
        if (separator[1]) {
            m = separator[1].match(/^\/(.*)\/$/);
            s = m ? RegExp(m[1]) : separator[1];
            data = data.map((e: string) => e.split(s));
        }
        return data;
    },
    replace: (data: any, search: string, replace: string = ""): string => {
        if (typeof data !== "string") return data;
        let m = search.match(/^\/(.*)\/([gimy])*$/);
        let s = m ? RegExp(m[1], m[2]) : search;
        return data.replace(s, replace);
    },
    lpad: (data: any, length: string, padding: string = " "): string => {
        const len = parseInt(length);
        if (typeof data !== "string" || isNaN(len) || data.length >= len) return data;
        return data.padStart(len, padding);
    },
    rpad: (data: any, length: string, padding: string = " "): string => {
        const len = parseInt(length);
        if (typeof data !== "string" || isNaN(len) || data.length >= len) return data;
        return data.padEnd(len, padding);
    },
    pad: (data: any, length: string, padding: string = " "): string => {
        const len = parseInt(length);
        if (typeof data !== "string" || isNaN(len) || data.length >= len) return data;
        const left = data.length + Math.floor((len - data.length) / 2);
        return data.padStart(left, padding).padEnd(len, padding);
    },
    line: (data: any, ...num: string[]): string => {
        if (typeof data !== "string" || !num.length) return data;
        const lines = data.split("\n");
        let res: string[] = [];
        num.forEach(e => {
            const m = e.split("-", 2);
            if (m && m.length === 2) {
                res = res.concat(lines.slice(parseInt(m[0]) - 1, parseInt(m[1])));
            } else res.push(lines[parseInt(e) - 1]);
        });
        return res.join("\n");
    },
    compare: (data: any, operator: ">" | ">=" | "<" | "<=" | "=" | "!=", check: string, lang?: string): boolean => {
        // test if type is date
        const parsedCheck = getDate(check, lang);
        const numCheck = Number(check);
        if (parsedCheck) {
            // date check
            const parsedData = data instanceof Date ? data : getDate(data, lang);
            if (!parsedData) return false;
            switch (operator) {
                case ">":
                    return parsedData > parsedCheck;
                case ">=":
                    return parsedData >= parsedCheck;
                case "<":
                    return parsedData < parsedCheck;
                case "<=":
                    return parsedData <= parsedCheck;
                case "=":
                    return parsedData == parsedCheck;
                case "!=":
                    return parsedData != parsedCheck;
            }
        } else if (!isNaN(numCheck)) {
            // number check
            const numData = Number(data);
            if (isNaN(numData)) return false;
            switch (operator) {
                case ">":
                    return numData > numCheck;
                case ">=":
                    return numData >= numCheck;
                case "<":
                    return numData < numCheck;
                case "<=":
                    return numData <= numCheck;
                case "=":
                    return numData == numCheck;
                case "!=":
                    return numData != numCheck;
            }
        } else {
            // string check
            switch (operator) {
                case "=":
                    return data == check;
                case "!=":
                    return data != check;
            }
            debug("Operation %s not allowed for string comparison", operator);
        }
        return false;
    }
};

function getDate(data: any, lang?: string) {
    let parsed = lang ? chrono.en.parse(data) : chrono.parse(data);
    if (parsed[0] && parsed[0].start) {
        // date check
        const start = parsed[0].start;
        //        if (parsed.knownValues.timezoneOffset === undefined) parsed.imply("timezoneOffset", moment().utcOffset());
        return start.date();
    }
    return undefined;
}

function getNamedElement(data: any, name: string, command: string): any {
    // negative numbers
    if (Array.isArray(data) && name.match(/^-\d+$/)) name = (data.length - 1 + parseInt(name)).toString();
    // direct name
    return command ? pathValue(data[name], command) : data[name];
}

function getElementList(data: any, select: string, command = ""): any[] | undefined {
    // no selection = select all
    if (!select) {
        select = Array.isArray(data) ? `0-${data.length - 1}` : Object.keys(data).join(",");
    }
    // negative selection
    let negative = false;
    if (select.substr(0, 1) === "!") {
        select = select.substr(1);
        negative = true;
    }
    // resolve range selection
    select = select.replace(/(\d+)-(\d+)/g, (_, from, to) => {
        const list = [];
        for (let count = from; count <= to; count++) {
            list.push(count);
        }
        return list.toString();
    });
    // get list of selections
    const list = select
        .split(/\s*,\s*/)
        .map(def => {
            // regexp
            let m = def.match(/^\/(.*)\/$/);
            if (m && typeof data === "object") {
                const re = m[1];
                if (negative)
                    return Object.keys(data)
                        .map(e => !e.match(new RegExp(re)))
                        .join(",");
                return Object.keys(data)
                    .map(e => e.match(new RegExp(re)))
                    .join(",");
            }
            return def;
        })
        .join(",")
        .split(",");
    // return element for single entry
    if (list.length == 1) return getNamedElement(data, list[0], command);
    // loop over selection
    const res: any = [];
    if (negative) {
        // negative loop
        if (Array.isArray(data)) {
            for (let count = 0; count < data.length; count++) {
                if (!list.includes(count.toString())) res.push(pathValue(data[count], command));
            }
        } else {
            Object.keys(data).forEach(e => {
                if (!list.includes(e)) res.push(pathValue(data[e], command));
            });
        }
    } else {
        list.forEach(e => res.push(pathValue(data[e], command)));
    }
    // done
    return res;
}

function getElementMap(data: any, select: string, command = ""): any[] {
    if (!select) select = Object.keys(data).join(",");
    // negative selection
    let negative = false;
    if (select.substr(0, 1) === "!") {
        select = select.substr(1);
        negative = true;
    }
    // get list of selections
    const list = select
        .split(/\s*,\s*/)
        .map(def => {
            // regexp
            let m = def.match(/^\/(.*)\/$/);
            if (m && typeof data === "object") {
                const re = m[1];
                if (negative)
                    return Object.keys(data)
                        .map(e => !e.match(new RegExp(re)))
                        .join(",");
                return Object.keys(data)
                    .map(e => e.match(new RegExp(re)))
                    .join(",");
            }
            return def;
        })
        .join(",")
        .split(",");
    // loop over selection
    const res: any = {};
    if (negative) {
        // negative loop
        Object.keys(data).forEach(e => {
            if (!list.includes(e)) res[e] = pathValue(data[e], command);
        });
    } else {
        list.forEach(e => (res[e] = pathValue(data[e], command)));
    }
    // done
    return res;
}

export default filter;
