// Merge Helper
// =================================================
// deep merging of data structures.

import Debug from 'debug';

import { clone, equal, empty } from './index';

const debug = Debug('data:merge');

export interface MergeInput {
    data?: any;
    base?: string;
    source?: any;
    // relations
    depend?: any[];
    alternate?: any[];
    // default combine methods
    object?: 'combine' | 'replace' | 'missing' | 'alternate'; // default: 'combine'
    array?: 'concat' | 'replace' | 'overwrite' | 'combine'; // default: 'concat'
    // overwrite per position
    pathObject?: { [key: string]: 'combine' | 'replace' | 'missing' | 'alternate' };
    pathArray?: { [key: string]: 'concat' | 'replace' | 'overwrite' | 'combine' };
    // general settings
    clone?: boolean; // = true
    allowFlags?: boolean; // = true
    map?: boolean;
}

interface Options {
    base?: string;
    source: any;
    path: string;
    // default combine methods
    object: 'combine' | 'replace' | 'missing' | 'alternate'; // default: 'combine'
    array: 'concat' | 'replace' | 'overwrite' | 'combine'; // default: 'concat'
    // overwrite per position
    pathObject: { [key: string]: 'combine' | 'replace' | 'missing' | 'alternate' };
    pathArray: { [key: string]: 'concat' | 'replace' | 'overwrite' | 'combine' };
    // general settings
    clone: boolean; // = true
    allowFlags: boolean; // = true
    map: boolean;
}

/**
 * Marge two or more data structures together.
 * @param list specification for each merge input with options:
 * - data - real data structures to be merged
 * - base - optional base path within the whole setup
 * - source - reference to source element or name
 * - object - general merge method: combine, replace, missing, alternate (default: combine)
 * - array - general merge method: concat, replace, overwrite, combine (default: concat)
 * - pathObject - method overwrite for specific paths
 * - pathArray - method overwrite for specific paths
 * - clone - set to true to make deep clone
 * - allowFlags - allow to overwrite in data structure
 * - map - generate map showing which result value comes from which source
 * @return merged object
 */
export function merge(...list: MergeInput[]): { data: any; map: any } {
    let data = {};
    const map = {};
    if (!list.length) return { data, map };
    // merge all
    list.forEach(e => {
        if (!e.data) return;
        // set defaults
        const opt: Options = {
            source: e.source || e.data,
            path: '',
            object: e.object || 'combine',
            array: e.array || 'concat',
            pathObject: e.pathObject || {},
            pathArray: e.pathArray || {},
            clone: e.clone === false ? false : true,
            allowFlags: e.allowFlags !== false,
            map: e.map || false
        };
        if (empty(e.data)) {
            debug('skip %s because not existing', e.source);
            return; // continue
        }
        if (e.depend && e.depend.length) {
            const missing = e.depend.filter(s => {
                let found = true;
                list.forEach(c => {
                    if (equal(s, c.source)) if (empty(c.data)) found = false;
                });
                return !found;
            });
            if (missing.length) {
                debug('skip %s because %o is not existing', e.source, missing);
                return; // continue
            }
        }
        if (e.alternate && e.alternate.length) {
            const found = e.alternate.filter(s => {
                let found = false;
                list.forEach(c => {
                    if (equal(s, c.source)) if (!empty(c.data)) found = true;
                });
                return found;
            });
            if (found.length) {
                debug('skip %s because %o is existing', e.source, found);
                return; // continue
            }
        }
        debug('start with target %o', data);
        debug('add source %s: %o', e.source, e.data);
        debug('options %o', {
            object: opt.object,
            array: opt.array,
            clone: opt.clone
        });
        if (e.base) e.data = setBase(e.base, e.data);
        data = deep(data, e.data, opt, map);
        if (e.map) debug('mapping %O', map);
    });
    debug('resulting to %O', data);
    return { data, map };
}

function setBase(path: string, data: any): any {
    const res: any = {};
    const m = path.match(/^([^\[\{}#]+?)(\.(.*)|([#\{\[].*))/);
    if (m) res[m[1]] = setBase(m[4] || m[3], data);
    else res[path] = data;
    return res;
}

function deep(target: any, source: any, opt: Options, map: any): any {
    const targetType = Array.isArray(target) ? 'array' : typeof target;
    const sourceType = Array.isArray(source) ? 'array' : typeof source;
    if (targetType === sourceType && targetType === 'array') {
        let method = opt.pathArray[opt.path] || opt.array;
        if (opt.allowFlags) {
            let last = source[source.length - 1];
            if (typeof last === 'string' && last.substr(0, 12) === 'MERGE_ARRAY_') {
                // @ts-ignore
                method = last.substr(12).toLowerCase();
                source.pop();
            }
        }
        // array settings
        if (method === 'replace') {
            if (
                target.map((e: any, i: number) => i).join(',') !==
                source.map((e: any, i: number) => i).join(',')
            ) {
                if (opt.map) {
                    if (!map.__map) map.__map = [opt.source, opt.path];
                    Object.keys(map).forEach(e => delete map[e]);
                    setSource(map, source, opt.source, opt.path);
                }
                target = opt.clone ? clone(source) : source;
            }
        } else if (method === 'overwrite') {
            if (opt.map && !map.__map) map.__map = [opt.source, opt.path];
            source.forEach((e: any, i: number) => {
                const subpath = `${opt.path}${opt.path ? '.' : ''}${i}`;
                if (opt.map) {
                    map[i] = {};
                    setSource(map[i], source[i], opt.source, subpath);
                }
                target[i] = opt.clone ? clone(e) : e;
            });
        } else if (method === 'combine') {
            if (opt.map && !map.__map) map.__map = [opt.source, opt.path];
            source.forEach((e: any, i: number) => {
                const subpath = `${opt.path}${opt.path ? '.' : ''}${i}`;
                if (opt.map && !map[i]) map[i] = {};
                target[i] = deep(target[i], source[i], { ...opt, path: subpath }, map[i]);
            });
        } else {
            // concat
            if (opt.map) {
                let pos = target.length;
                source.forEach((e: any, i: number) => {
                    const subpath = `${opt.path}${opt.path ? '.' : ''}${i}`;
                    if (!map[pos]) map[pos] = {};
                    setSource(map[pos++], source[i], opt.source, subpath);
                });
            }
            target = target.concat(opt.clone ? clone(source) : source);
        }
    } else if (targetType === sourceType && targetType === 'object') {
        let method = opt.pathObject[opt.path] || opt.object;
        if (opt.allowFlags && typeof source.MERGE_OBJECT === 'string') {
            // @ts-ignore
            method = source.MERGE_OBJECT;
            delete source.MERGE_OBJECT;
        }
        // object merging
        if (method === 'replace') {
            const tk = Object.keys(target)
                .sort()
                .join(',');
            const sk = Object.keys(source)
                .sort()
                .join(',');
            if (tk !== sk) target = replaceObject(target, source, opt, map);
        } else if (method === 'alternate') {
            if (!Object.keys(target).length) target = replaceObject(target, source, opt, map);
        } else if (method === 'missing') {
            Object.keys(source).forEach(k => {
                if (!target[k]) replaceObjectKey(target, source, opt, map, k);
            });
        } else {
            // combine
            Object.keys(source).forEach(k => {
                if (target[k]) {
                    if (opt.map) {
                        if (!map.__map) map.__map = [opt.source, opt.path];
                        if (!map[k]) map[k] = {};
                    }
                    const subpath = `${opt.path}${opt.path ? '.' : ''}${k}`;
                    target[k] = deep(
                        target[k],
                        source[k],
                        { ...opt, path: subpath },
                        opt.map ? map[k] : map
                    );
                } else replaceObjectKey(target, source, opt, map, k);
            });
        }
    } else if (target !== source) target = replaceObject(target, source, opt, map);
    // return result
    return target;
}

function replaceObject(target: any, source: any, opt: Options, map: any): any {
    if (opt.map) {
        Object.keys(map).forEach(e => delete map[e]);
        setSource(map, source, opt.source, opt.path);
    }
    return opt.clone ? clone(source) : source;
}

function replaceObjectKey(target: any, source: any, opt: Options, map: any, k: any): void {
    const subpath = `${opt.path}${opt.path ? '.' : ''}${k}`;
    if (opt.map) {
        if (!map.__map) map.__map = [opt.source, opt.path];
        map[k] = {};
        setSource(map[k], source[k], opt.source, subpath);
    }
    target[k] = opt.clone ? clone(source[k]) : source[k];
}

function setSource(map: any, data: any, source: any, path: string): void {
    map.__map = [source, path];
    if (Array.isArray(data)) {
        data.forEach((e, i) => {
            map[i] = {};
            setSource(map[i], e, source, `${path}${path ? '.' : ''}${i}`);
        });
    } else if (typeof data === 'object') {
        Object.keys(data).forEach(k => {
            map[k] = {};
            setSource(map[k], data[k], source, `${path}${path ? '.' : ''}${k}`);
        });
    }
}

export default { merge };
